package com.example.gitmobius.usecase

import android.widget.LinearLayout
import com.example.gitmobius.network.GitAPI
import com.example.gitmobius.views.GitView
import dagger.hilt.android.scopes.ActivityScoped
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

@ActivityScoped
internal class GitTrendingUsecase @Inject constructor(
    private val gitAPI: GitAPI,
    private val gitView: GitView
) {

    internal fun fetchAndShowTrendingRepos(linearLayout: LinearLayout): Disposable {
        gitView.showLoading(linearLayout)
        return gitAPI.getTrendingRepos()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSuccess { gitView.showGitItems(linearLayout, it) }
            .doOnError { gitView.showGitError(it) }
            .subscribe({
                Timber.i("Item list fetched")
            }, {
                Timber.e(it)
            })

    }
}
