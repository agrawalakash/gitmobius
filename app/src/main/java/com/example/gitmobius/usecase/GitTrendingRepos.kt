package com.example.gitmobius.usecase

import com.google.gson.annotations.SerializedName

data class GitTrendingRepos(
    @SerializedName("author") val author: String,
    @SerializedName("name") val name : String,
    @SerializedName("avatar") val imageUrl: String,
    @SerializedName("url") val url: String,
    @SerializedName("description") val description: String
)
