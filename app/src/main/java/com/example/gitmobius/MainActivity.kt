package com.example.gitmobius

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.gitmobius.databinding.ActivityMainBinding
import com.example.gitmobius.usecase.GitTrendingUsecase
import dagger.hilt.android.AndroidEntryPoint
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    @Inject
    internal lateinit var gitTrendingUsecase: GitTrendingUsecase

    @Inject
    internal lateinit var compositeDisposable: CompositeDisposable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        compositeDisposable.add(
                gitTrendingUsecase.fetchAndShowTrendingRepos(binding.llGit)
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }
}
