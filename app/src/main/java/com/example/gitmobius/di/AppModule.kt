package com.example.gitmobius.di

import com.example.gitmobius.network.GitAPI
import com.example.gitmobius.views.GitView
import com.example.gitmobius.views.GitViewImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.scopes.ActivityScoped
import io.reactivex.disposables.CompositeDisposable
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


/**
 * Module to tell Hilt how to provide instances of types that cannot be constructor-injected.
 *
 * As these types are scoped to the application lifecycle using @Singleton, they're installed
 * in Hilt's ApplicationComponent.
 */
@Module
@InstallIn(ActivityComponent::class)
abstract class AppModule {

    companion object {
        @ActivityScoped
        @Provides
        fun provideRetrofitInstance(): Retrofit {
            return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(OkHttpClient())
                .baseUrl("https://ghapi.huchen.dev" + "/")
                .build()
        }

        @ActivityScoped
        @Provides
        fun providesGitAPI(retrofit: Retrofit): GitAPI {
            return retrofit.create(GitAPI::class.java)
        }

        @ActivityScoped
        @Provides
        fun compositeDisposable(): CompositeDisposable {
            return CompositeDisposable()
        }
    }

    @Binds
    @ActivityScoped
    internal abstract fun gitView(
        gitViewImpl: GitViewImpl
    ): GitView

}

