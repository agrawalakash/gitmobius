package com.example.gitmobius.network

import com.example.gitmobius.usecase.GitTrendingRepos
import io.reactivex.Single
import retrofit2.http.GET

interface GitAPI {
    @GET("repositories")
    fun getTrendingRepos() : Single<List<GitTrendingRepos>>
}
