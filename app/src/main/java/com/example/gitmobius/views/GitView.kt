package com.example.gitmobius.views

import android.util.Log
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.gitmobius.R
import com.example.gitmobius.usecase.GitTrendingRepos
import dagger.hilt.android.scopes.ActivityScoped
import javax.inject.Inject


interface GitView {
    fun showLoading(linearLayout: LinearLayout)
    fun showGitItems(linearLayout: LinearLayout, gitRepos: List<GitTrendingRepos>)
    fun showGitError(throwable: Throwable)
}

@ActivityScoped
class GitViewImpl @Inject constructor() : GitView {
    override fun showLoading(linearLayout: LinearLayout) {
            linearLayout.removeAllViews()
            LayoutInflater.from(linearLayout.context).inflate(R.layout.shimmer_git_item, linearLayout)
    }

    override fun showGitItems(linearLayout: LinearLayout, gitRepos: List<GitTrendingRepos>) {
        linearLayout.removeAllViews()
        LayoutInflater.from(linearLayout.context)
            .inflate(R.layout.git_recycler_view, linearLayout)
        with(linearLayout.findViewById<RecyclerView>(R.id.recycler_view)) {
            adapter = GitAdapter(gitRepos)
            layoutManager =
                androidx.recyclerview.widget.LinearLayoutManager(context)
        }
    }

    override fun showGitError(throwable: Throwable) {
        Log.e("Error Showing Git Items", throwable.toString())
    }
}
