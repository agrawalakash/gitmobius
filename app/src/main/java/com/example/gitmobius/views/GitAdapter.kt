package com.example.gitmobius.views

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import com.example.gitmobius.R
import com.example.gitmobius.usecase.GitTrendingRepos
import kotlinx.android.synthetic.main.git_item.view.*

class GitAdapter(
    private val gitRepos: List<GitTrendingRepos>,
    private val onClickListener: ((Int) -> Unit)? = null
) : RecyclerView.Adapter<GitAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder((LayoutInflater.from(parent.context).inflate(R.layout.git_item, parent,false)))

    override fun getItemCount(): Int = gitRepos.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.onBind(gitRepos[position], position)

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal fun onBind(gitRepository: GitTrendingRepos, position: Int) {
            with(itemView){
                tv_repo_name.text = gitRepository.name
                tv_repo_author.text = gitRepository.author
                tv_repo_description.text = gitRepository.description
                tv_repo_url.text = gitRepository.url
            }
        }
    }
}
